;(function (document, $) {
	'use strict';

    //Load ondocumentready functions
    $(function () {
        initAutoScroll();
		togglePageScrollHandler();
    });

	var scrollSpeed = 10,
		scrollDelay = 2000,
    	autoScroll = true,
		scroller,
        pageScroll = function () {
			var pauzed = localStorage.getItem('pauze');
			if(pauzed === 'true' || pauzed === null) {
				return;
			}

            window.scrollBy(1,0);

			//When scroll reaches end of window.
            if (Math.round($(document).scrollLeft()) == ( $(document).width() - $(window).width()) ) {
				setTimeout(function () {
					window.scrollTo(0,0);
					setTimeout(function() {
						scroller = setTimeout(pageScroll,scrollSpeed);
					}, scrollDelay);
				}, scrollDelay);
            } else {
				scroller = setTimeout(pageScroll,scrollSpeed);
			}
        },

        initAutoScroll = function () {
            if(!autoScroll && localStorage.getItem('pauze') !== false) {
                return;
            }

            pageScroll();
        },

		togglePageScrollHandler = function () {
			$('.js-pauze-scroll').on('click', function () {
				var $pauzeLink = $(this);
				var pauzed = localStorage.getItem('pauze');
				if(pauzed === 'true' || pauzed === null) {
					scroller = setTimeout(pageScroll,scrollSpeed);
					localStorage.setItem('pauze', false);
					$pauzeLink.text('Pauze');
				} else {
					clearTimeout(scroller);
					localStorage.setItem('pauze', true);
					$pauzeLink.text('Play');
				}
			});
		};

}(document, jQuery));
